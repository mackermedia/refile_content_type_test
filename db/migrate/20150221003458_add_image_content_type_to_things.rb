class AddImageContentTypeToThings < ActiveRecord::Migration
  def change
    add_column :things, :image_content_type, :string
  end
end
