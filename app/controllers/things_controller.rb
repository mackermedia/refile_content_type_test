class ThingsController < ApplicationController

  def index
    @new_thing = Thing.new

    @things = Thing.all
  end

  def create
    thing = Thing.new(params.require(:thing).permit(:image))

    if thing.save
      redirect_to things_url
    else
      render :index
    end
  end
end
